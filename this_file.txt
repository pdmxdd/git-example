they did work over here on a file that you don't touch or worry about

however, this changes the history of master, so it's no longer in a linear point with your feature branch.

you can rebase master onto your branch and then it will be linear again and you can fast-forward merge!
